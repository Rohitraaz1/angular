export class Promotion{
    id:string;
    label:string;
    image:string;
    name:string;
    price:string;
    featured:boolean;
    description:string;
}