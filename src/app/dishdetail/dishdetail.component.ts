import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { DishService } from '../services/dish.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Dish } from '../shared/dish';
import { switchMap } from 'rxjs/operators';
import { Comment } from '../shared/comment';
import { visibility, flyInOut, expand } from '../animations/app.animations';




@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host: {
    '[@flyInOut]' : 'true',
    'style': 'display: block'
  },
  animations: [
    visibility(), flyInOut(), expand()
  ]
})
export class DishdetailComponent implements OnInit {

  @ViewChild('fformc') commentsFormDirective: { resetForm: () => void; };
  commentsForm: FormGroup;
  ratingcomment: Comment;
  dishIds: string[];
  prev: string;
  next: string;
  dish: Dish;
  errMess: string;
  dishcopy: Dish;
  visibility: 'shown';

  constructor(
    private dishservice: DishService,
    private route: ActivatedRoute,
    private location: Location,
    private fb1: FormBuilder,
    @Inject('BaseURL') private BaseURL
  ) { this.createForm(); }
  ngOnInit() {
    this.dishservice.getDishIds().subscribe(dishIds => this.dishIds = dishIds);

    this.route.params.pipe(switchMap((params: Params) => {  return this.dishservice.getDish(+params['id']); }))
      .subscribe(dish => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; },
        errmess => this.errMess = <any>errmess);
  }
  formErrors = {
    'author': '',
    'comment': ''
  };
  validationMessages = {
    'author': {
      'required': 'Author Name is required.',
      'minlength': 'Author Name must be at least 2 characters long.',
      'maxlength': 'Author Name cannot be more than 25 characters long.'
    },
    'comment': {
      'required': 'Comments are required.'
    },
  };
  createForm(): void {
    this.commentsForm = this.fb1.group({
      author: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      rating: ['5'],
      comment: ['', [Validators.required]]
    });
    this.commentsForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged();  // (re)set validation messages now
  }
  onSubmit(_ratingcomment: any) {
    this.ratingcomment = this.commentsForm.value;
    this.ratingcomment.date = new Date().toISOString();
    console.log(this.ratingcomment);
    this.dishcopy.comments.push(this.ratingcomment);
    this.dishservice.putDishes(this.dishcopy)
      .subscribe(dish => {
        this.dish = dish; this.dishcopy = dish;
      },
        errmess => { this.dish = null; this.dishcopy = null; this.errMess = <any>errmess; });
    this.commentsForm.reset({
      author: '',
      rating: 5,
      comment: ''
    });
    this.commentsFormDirective.resetForm();
  }
  onValueChanged(data?: any) {
    if (!this.commentsForm) { return; }
    const form = this.commentsForm;
    for (const field in this.commentsForm) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.commentsForm[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  setPrevNext(dishId: string) {
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
  }

  goBack(): void {
    this.location.back();
  }

}
